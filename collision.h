#include <stdbool.h>

#include "sprite.h"
#include "constants.h"

bool is_colliding(Sprite *spriteA, Sprite *spriteB){
    if(
        spriteA->x + spriteA->width >= spriteB->x &&
        spriteB->x + spriteB->width >= spriteA->x &&
        spriteA->y + spriteA->height >= spriteB->y &&
        spriteB->y + spriteB->height >= spriteA->y
    )
        return true;
    return false;
}

bool will_collide_with_window(Sprite *sprite, float delta){
    float future_pos = (sprite->vel_y * delta) + sprite->y;

    if(
        future_pos > 0 &&
        future_pos + sprite->height < WINDOW_HEIGHT
    )
        return true;

    return false;
}

bool is_inside(Sprite *parent, Sprite *child){
    if(
        child->height + child->y <= parent->height + parent->y &&
        child->width + child->x <= parent->width + parent->x &&
        child->x >= parent->x &&
        child->y >= parent->y
    )
        return true;
    return false;
}

bool is_inside_window(Sprite *sprite){
    if(
        sprite->height + sprite->y <= WINDOW_HEIGHT &&
        sprite->width + sprite->x <= WINDOW_WIDTH &&
        sprite->x >= 0 &&
        sprite->y >= 0
    )
        return true;
    return false;
}