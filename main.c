#include <stdio.h>
#include <time.h>
#include <SDL2/SDL.h>

#include "collision.h"
#include "constants.h"
#include "sprite.h"

// SDL Variables.

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

// Sprites.

Sprite ball;
Sprite right_player;
Sprite left_player;

// Game Loop Variables.

int is_game_running = FALSE;
int last_frame_time = 0;

void setup(){

    // Seeding random function.

    srand(time(NULL));

    // Creating Colors and Sprites.

    SDL_Color red = {255, 0, 0, 255};
    SDL_Color white = {255, 255, 255, 255};

    ball = create_ball(red);
    right_player = create_player('R', white);
    left_player = create_player('L', white);

}

void process_input(){
    SDL_Event event;
    SDL_PollEvent(&event);

    const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );

    if (currentKeyStates[SDL_SCANCODE_ESCAPE])
        is_game_running = FALSE;
    if (currentKeyStates[SDL_SCANCODE_UP])
        left_player.vel_y = -400;
    if (currentKeyStates[SDL_SCANCODE_DOWN])
        left_player.vel_y = +400;
    if (currentKeyStates[SDL_SCANCODE_W])
        right_player.vel_y = -400;
    if (currentKeyStates[SDL_SCANCODE_S])
        right_player.vel_y = +400;
}

void update(){

    // Sleep until frame target time was reached.

    while (!SDL_TICKS_PASSED(SDL_GetTicks(), last_frame_time + FRAME_TARGET_TIME));

    // Calculating Delta Time.

    float delta_time = (SDL_GetTicks() - last_frame_time) / 1000.0f;
    last_frame_time = SDL_GetTicks();

    // Collision with top and bottom walls.

    if(ball.height + ball.y > WINDOW_HEIGHT || ball.y < 0) {
        ball.vel_y = -ball.vel_y;
    } else {

        // Game Over.

        if(!is_inside_window(&ball))
            is_game_running = FALSE;

    }

    // Check for Collisions between ball and players.

    if(is_colliding(&left_player, &ball) || is_colliding(&right_player, &ball))
        ball.vel_x = -ball.vel_x * 1.1;

    // Update Ball Position.

    ball.x += ball.vel_x * delta_time;
    ball.y += ball.vel_y * delta_time;

    // If players aren't colliding with walls, update position.

    if(will_collide_with_window(&left_player, delta_time))
        left_player.y = (left_player.vel_y * delta_time) + left_player.y;

    if(will_collide_with_window(&right_player, delta_time))
        right_player.y = (right_player.vel_y * delta_time) + right_player.y;

    // Restoring Player's velocity.

    right_player.vel_y = 0;
    left_player.vel_y = 0;

}

void render_object(Sprite *object) {
    SDL_SetRenderDrawColor(renderer, object->color.r, object->color.g, object->color.b, object->color.a);
    SDL_Rect ball_rect = {
        (int) object->x,
        (int) object->y,
        (int) object->width,
        (int) object->height
    };
    SDL_RenderFillRect(renderer, &ball_rect);
}

void render_scene() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
}

void render(){
    render_scene();
    render_object(&ball);
    render_object(&right_player);
    render_object(&left_player);
    SDL_RenderPresent(renderer);
}

void destroy_window(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

int initWindow(void){

    if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        fprintf(stderr, "Error Initializing SDL.");
        return FALSE;
    }

    window = SDL_CreateWindow(
        "Ping Pong",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH,
        WINDOW_HEIGHT,
        SDL_WINDOW_BORDERLESS
    );

    if(!window) {
        fprintf(stderr, "Error Initializing SDL Windows.");
        return FALSE;
    }

    renderer = SDL_CreateRenderer(window, -1, 0);

    if(!renderer){
        fprintf(stderr, "Error Initializing SDL Renderer");
        return FALSE;
    }

    return TRUE;

}

int main(void) {
    is_game_running = initWindow();
    setup();

    while(is_game_running) {
        process_input();
        update();
        render();
    }

    destroy_window();
    return TRUE;
}
