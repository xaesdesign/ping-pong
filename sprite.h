#include <SDL2/SDL.h>
#include "constants.h"

#ifndef SPRITE_HEADER_H
#define SPRITE_HEADER_H

typedef struct sprite_struct {
    float x;
    float y;
    float width;
    float height;
    float vel_x;
    float vel_y;
    SDL_Color color;
} Sprite;

int generate_velocity(int vel){
    return vel * ((rand() & 1) ? 1 : -1);
}

Sprite create_sprite(float x, float y, float w, float h, float vel_x, float vel_y, SDL_Color color){
    Sprite new_sprite;

    new_sprite.width = w;
    new_sprite.height = h;
    new_sprite.y = y;
    new_sprite.x = x;
    new_sprite.vel_x = vel_x;
    new_sprite.vel_y = vel_y;
    new_sprite.color = color;

    return new_sprite;
}

Sprite create_player(char position, SDL_Color color){
    float width = 20;
    float height = 100;
    float y = (WINDOW_HEIGHT / 2) - (height / 2);
    float x = (position == 'R') ? 0 : WINDOW_WIDTH - width;
    return create_sprite(x, y, width, height, 0, 0, color);
}

Sprite create_ball(SDL_Color color) {
    float width = 20;
    float height = 20;
    float y = (WINDOW_HEIGHT / 2) - (height / 2);
    float x = (WINDOW_WIDTH / 2) - (width / 2);
    float vel_x = generate_velocity(200);
    float vel_y = generate_velocity(200);
    return create_sprite(x, y, width, height, vel_x, vel_y, color);
}

#endif